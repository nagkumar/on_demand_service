#!/usr/bin/python
import sys
import datetime
from pymongo import MongoClient
import random
import string
from migration_transformer import MigrationTransformer

client = MongoClient()
db = client.log_database
db.add_son_manipulator(MigrationTransformer())
collection = db.log

print "Number of args: ", len(sys.argv)
print "Args list: ", str(sys.argv)

end_date = days = no_of_businesses = no_of_workers = no_of_users = frequency = None


def get_random_search_term():
    i = random.randint(0, 9)
    if i % 2 == 0:
        return ''.join(random.choice(string.ascii_lowercase) for j in range(5))
    return ""


PAGES = ['home', 'business', 'worker', 'worker_schedule', 'book']


def get_random_page_name():
    i = random.randint(0, 4)
    return PAGES[i]


def encode_time(value):
    return datetime.datetime.combine(value, datetime.datetime.min.time())


try:
    # start_date = datetime.datetime.strptime(sys.argv[1], "%H:%M:%S-%m-%d-%Y")
    start_date = datetime.datetime.now() - datetime.timedelta(days=15)
    # if end_date == 'now':
    end_date = datetime.datetime.now()
    # else:
    #     end_date = datetime.datetime.strptime(end_date, "%H:%M:%S-%m-%d-%Y")
    no_of_businesses = 2
    no_of_workers = 4
    no_of_users = 250
    frequency = 60
    # raise IndexError
    temp_date = start_date
    while temp_date <= end_date:
        # add user log for number of users
        # for user in range(1, no_of_users + 1):
        #     for business in range(1, no_of_businesses + 1):
        #         for worker in range(1, no_of_users + 1):
        #             # add to log
        #             page_name = get_random_page_name()
        #             if page_name == PAGES[0]:
        #                 search_term = get_random_search_term()
        #             else:
        #                 search_term = ""
        #             l = {
        #                 "log_date": encode_time(temp_date.date()),
        #                 "log_timestamp": temp_date,
        #                 "user_id": user,
        #                 "worker_id": worker,
        #                 "business_id": business,
        #                 "search_term": search_term,
        #                 "page_name": page_name,
        #                 "action": "page_load",
        #                 "user_agent": "",
        #                 "ip_address": "10.1.0.%s" % user,
        #                 "referrer": ""
        #             }
        #             collection.insert_one(l)
        #             temp_date += datetime.timedelta(seconds=frequency)
        user = random.randint(1, no_of_users)
        business = random.randint(1, no_of_businesses)
        if business == 1:
            worker = random.randint(1, 4)
        else:
            worker = random.randint(5, (5+no_of_workers))
        page_name = get_random_page_name()
        if page_name == PAGES[0]:
            search_term = get_random_search_term()
        else:
            search_term = ""
        registered_flag = random.randint(1, 4)
        if registered_flag % 3 != 0:
            l = {
                "log_date": encode_time(temp_date.date()),
                "log_timestamp": temp_date,
                "user_id": user,
                "worker_id": worker,
                "business_id": business,
                "search_term": search_term,
                "page_name": page_name,
                "action": "page_load",
                "user_agent": "",
                "ip_address": "10.1.0.%s" % user,
                "referrer": ""
            }
            collection.insert_one(l)
        else:
            l = {
                "log_date": encode_time(temp_date.date()),
                "log_timestamp": temp_date,
                "user_id": "",
                "worker_id": worker,
                "business_id": business,
                "search_term": search_term,
                "page_name": page_name,
                "action": "page_load",
                "user_agent": "",
                "ip_address": "10.1.1.%s" % user,
                "referrer": ""
            }
            collection.insert_one(l)
        temp_date += datetime.timedelta(seconds=frequency)

except (IndexError, ValueError) as e:
    print e
    print "Incorrect usage."
    print "Proper usage is: "
    print "python insert_temp_data.py <start_date> <end_date> <no_of_businesses> <no_of_workers> <no_of_users> <frequency>"
    print "date format = [now, hh:mm:ss MM-DD-yyyy]"
    print "Frequency values = <value> seconds"
