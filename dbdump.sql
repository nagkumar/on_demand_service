CREATE DATABASE  IF NOT EXISTS `project` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `project`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: 127.0.0.1    Database: project
-- ------------------------------------------------------
-- Server version	5.6.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `slot_id` varchar(10) NOT NULL DEFAULT '',
  `date` date DEFAULT NULL,
  `day` varchar(10) DEFAULT NULL,
  `begin_time` int(4) DEFAULT NULL,
  `end_time` int(4) DEFAULT NULL,
  PRIMARY KEY (`slot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `service_public`
--

DROP TABLE IF EXISTS `service_public`;
/*!50001 DROP VIEW IF EXISTS `service_public`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `service_public` (
  `name` tinyint NOT NULL,
  `description` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `worker`
--

DROP TABLE IF EXISTS `worker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker` (
  `worker_id` varchar(10) NOT NULL DEFAULT '',
  `sm_id` varchar(10) NOT NULL DEFAULT '',
  `password` varchar(20) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`worker_id`,`sm_id`),
  KEY `sm_id` (`sm_id`),
  CONSTRAINT `worker_ibfk_1` FOREIGN KEY (`sm_id`) REFERENCES `small_business` (`sm_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker`
--

LOCK TABLES `worker` WRITE;
/*!40000 ALTER TABLE `worker` DISABLE KEYS */;
INSERT INTO `worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('1','1','abc123','Capt. Piccard'),('2','1','abc123','James Kirk'),('3','1','abc123','Leonard McCoy'),('4','1','abc123','Hikara Sulu'),('5','2','bcd123','Darth Vader'),('6','2','bcd123','Luke Skywalker'),('7','2','bcd123','Princess Leia'),('8','2','bcd123','Han Solo');
/*!40000 ALTER TABLE `worker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `small_business_activation_log`
--

DROP TABLE IF EXISTS `small_business_activation_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_business_activation_log` (
  `sm_id` varchar(10) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  `activate` tinyint(1) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `small_business_activation_log`
--

LOCK TABLES `small_business_activation_log` WRITE;
/*!40000 ALTER TABLE `small_business_activation_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `small_business_activation_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `worker_id` int(11) DEFAULT NULL,
  `small_business_id` int(11) DEFAULT NULL,
  `search_term` varchar(45) DEFAULT NULL,
  `page_name` varchar(45) DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `referrrer` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `contractor_list`
--

DROP TABLE IF EXISTS `contractor_list`;
/*!50001 DROP VIEW IF EXISTS `contractor_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contractor_list` (
  `name` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `mobile` tinyint NOT NULL,
  `street_address` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `zip` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `service_recipient`
--

DROP TABLE IF EXISTS `service_recipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_recipient` (
  `email` varchar(40) NOT NULL DEFAULT '',
  `name` varchar(30) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `street_address` varchar(25) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(15) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `mobile` bigint(8) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_recipient`
--

LOCK TABLES `service_recipient` WRITE;
/*!40000 ALTER TABLE `service_recipient` DISABLE KEYS */;
INSERT INTO `service_recipient` (`email`, `name`, `password`, `street_address`, `city`, `state`, `zip`, `mobile`) VALUES ('hayden@christensen.com','Hayden Christensen','hay123','Death Star','San Jose','CA',95112,4564564567),('leonard.nimoy@nimoy.com','Leanord Nimoy','leo123','Vulcan Government','San Jose','CA',95112,7897897890),('natalie.portman@portman.com','Natalie Portman','nat123','Galactic Senate','San Jose','CA',95112,5675675678),('wil.wheaton@wheaton.com','Will Wheaton','wil123','United Federation of Plan','San Jose','CA',95112,6786786789);
/*!40000 ALTER TABLE `service_recipient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_record`
--

DROP TABLE IF EXISTS `service_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_record` (
  `record_id` varchar(10) NOT NULL DEFAULT '',
  `worker_id` varchar(10) DEFAULT NULL,
  `sm_id` varchar(10) DEFAULT NULL,
  `slot_id` varchar(10) DEFAULT NULL,
  `service_id` varchar(10) DEFAULT NULL,
  `service_recipient` varchar(40) DEFAULT NULL,
  `service_status` varchar(10) DEFAULT NULL,
  `rating` int(1) DEFAULT NULL,
  `admin_review` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`record_id`),
  KEY `sm_id` (`sm_id`),
  KEY `worker_id` (`worker_id`),
  KEY `slot_id` (`slot_id`),
  KEY `service_id` (`service_id`),
  KEY `service_recipient` (`service_recipient`),
  CONSTRAINT `service_record_ibfk_1` FOREIGN KEY (`sm_id`) REFERENCES `small_business` (`sm_id`),
  CONSTRAINT `service_record_ibfk_2` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`worker_id`),
  CONSTRAINT `service_record_ibfk_3` FOREIGN KEY (`slot_id`) REFERENCES `schedule` (`slot_id`),
  CONSTRAINT `service_record_ibfk_4` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`),
  CONSTRAINT `service_record_ibfk_5` FOREIGN KEY (`service_recipient`) REFERENCES `service_recipient` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_record`
--

LOCK TABLES `service_record` WRITE;
/*!40000 ALTER TABLE `service_record` DISABLE KEYS */;
INSERT INTO `service_record` (`record_id`, `worker_id`, `sm_id`, `slot_id`, `service_id`, `service_recipient`, `service_status`, `rating`, `admin_review`) VALUES ('1','1','1','1','1','1','1',NULL,0);
/*!40000 ALTER TABLE `service_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_provider`
--

DROP TABLE IF EXISTS `service_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_provider` (
  `worker_id` varchar(10) NOT NULL DEFAULT '',
  `sm_id` varchar(10) NOT NULL DEFAULT '',
  `service_id` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`sm_id`,`worker_id`,`service_id`),
  KEY `worker_id` (`worker_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_provider_ibfk_1` FOREIGN KEY (`sm_id`) REFERENCES `small_business` (`sm_id`) ON DELETE CASCADE,
  CONSTRAINT `service_provider_ibfk_2` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`worker_id`) ON DELETE CASCADE,
  CONSTRAINT `service_provider_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_provider`
--

LOCK TABLES `service_provider` WRITE;
/*!40000 ALTER TABLE `service_provider` DISABLE KEYS */;
INSERT INTO `service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('1','1','1'),('1','1','4'),('2','1','2'),('3','1','2'),('4','1','4'),('5','2','1'),('5','2','4'),('6','2','2'),('7','2','3'),('8','2','4');
/*!40000 ALTER TABLE `service_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `service_record_public`
--

DROP TABLE IF EXISTS `service_record_public`;
/*!50001 DROP VIEW IF EXISTS `service_record_public`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `service_record_public` (
  `record_id` tinyint NOT NULL,
  `Worker` tinyint NOT NULL,
  `Business` tinyint NOT NULL,
  `Business_email` tinyint NOT NULL,
  `Date` tinyint NOT NULL,
  `Service` tinyint NOT NULL,
  `Status` tinyint NOT NULL,
  `Rating` tinyint NOT NULL,
  `customer` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `review_log`
--

DROP TABLE IF EXISTS `review_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_log` (
  `name` varchar(10) NOT NULL DEFAULT '',
  `sm_id` varchar(10) DEFAULT NULL,
  `review` varchar(200) DEFAULT NULL,
  `approve_status` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `sm_id` (`sm_id`),
  CONSTRAINT `review_log_ibfk_1` FOREIGN KEY (`sm_id`) REFERENCES `small_business` (`sm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_log`
--

LOCK TABLES `review_log` WRITE;
/*!40000 ALTER TABLE `review_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `review_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `service_id` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` (`service_id`, `name`, `description`) VALUES ('1','Carpenter','One who deals with wood'),('2','Nanny','One who takes care of your kids'),('3','Cleaning','One who clears your mess'),('4','Plumbing','One who fixes your taps');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `first_name` varchar(10) DEFAULT NULL,
  `last_name` varchar(10) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`admin_id`, `first_name`, `last_name`, `password`) VALUES (0,'Admin','Admin','admin123'),(2,'admin','admin','admin123');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `small_business`
--

DROP TABLE IF EXISTS `small_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_business` (
  `sm_id` varchar(10) NOT NULL DEFAULT '',
  `email` varchar(40) NOT NULL DEFAULT '',
  `password` varchar(20) DEFAULT NULL,
  `street_address` varchar(25) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(15) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `mobile` bigint(8) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sm_id`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `small_business`
--

LOCK TABLES `small_business` WRITE;
/*!40000 ALTER TABLE `small_business` DISABLE KEYS */;
INSERT INTO `small_business` (`sm_id`, `email`, `password`, `street_address`, `city`, `state`, `zip`, `mobile`, `name`, `activate`) VALUES ('1','business1@business1.com','business123','One washington square','San Jose','CA',95113,1231231234,'Business1Nanny',1),('2','business1@business1.com','business123','One washington square','San Jose','CA',95113,2342342345,'Business2',0);
/*!40000 ALTER TABLE `small_business` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_availability`
--

DROP TABLE IF EXISTS `worker_availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_availability` (
  `worker_id` varchar(10) NOT NULL DEFAULT '',
  `sm_id` varchar(10) NOT NULL DEFAULT '',
  `service_id` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`sm_id`,`worker_id`,`service_id`),
  KEY `worker_id` (`worker_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `worker_availability_ibfk_1` FOREIGN KEY (`sm_id`) REFERENCES `small_business` (`sm_id`) ON DELETE CASCADE,
  CONSTRAINT `worker_availability_ibfk_2` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`worker_id`) ON DELETE CASCADE,
  CONSTRAINT `worker_availability_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `schedule` (`slot_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_availability`
--

LOCK TABLES `worker_availability` WRITE;
/*!40000 ALTER TABLE `worker_availability` DISABLE KEYS */;
/*!40000 ALTER TABLE `worker_availability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `customer_public`
--

DROP TABLE IF EXISTS `customer_public`;
/*!50001 DROP VIEW IF EXISTS `customer_public`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `customer_public` (
  `email` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `mobile` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `service_public`
--

/*!50001 DROP TABLE IF EXISTS `service_public`*/;
/*!50001 DROP VIEW IF EXISTS `service_public`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `service_public` AS (select `service`.`name` AS `name`,`service`.`description` AS `description` from `service`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contractor_list`
--

/*!50001 DROP TABLE IF EXISTS `contractor_list`*/;
/*!50001 DROP VIEW IF EXISTS `contractor_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contractor_list` AS select `small_business`.`name` AS `name`,`small_business`.`email` AS `email`,`small_business`.`mobile` AS `mobile`,`small_business`.`street_address` AS `street_address`,`small_business`.`city` AS `city`,`small_business`.`state` AS `state`,`small_business`.`zip` AS `zip` from `small_business` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `service_record_public`
--

/*!50001 DROP TABLE IF EXISTS `service_record_public`*/;
/*!50001 DROP VIEW IF EXISTS `service_record_public`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `service_record_public` AS select `rec`.`record_id` AS `record_id`,`worker`.`name` AS `Worker`,`sm`.`name` AS `Business`,`sm`.`email` AS `Business_email`,`slot`.`date` AS `Date`,`service`.`name` AS `Service`,`rec`.`service_status` AS `Status`,`rec`.`rating` AS `Rating`,`rec`.`service_recipient` AS `customer` from ((((`worker` join `schedule` `slot`) join `small_business` `sm`) join `service`) join `service_record` `rec`) where ((`rec`.`worker_id` = `worker`.`worker_id`) and (`rec`.`slot_id` = `slot`.`slot_id`) and (`rec`.`sm_id` = `sm`.`sm_id`) and (`rec`.`service_id` = `service`.`service_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `customer_public`
--

/*!50001 DROP TABLE IF EXISTS `customer_public`*/;
/*!50001 DROP VIEW IF EXISTS `customer_public`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `customer_public` AS select `service_recipient`.`email` AS `email`,`service_recipient`.`name` AS `name`,`service_recipient`.`mobile` AS `mobile` from `service_recipient` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 11:19:55
