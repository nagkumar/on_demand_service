INSERT INTO `project`.`admin` (`admin_id`, `first_name`, `last_name`) VALUES ('admin', 'Admin', 'Admin');
INSERT INTO `project`.`service` (`service_id`, `name`, `description`) VALUES ('1', 'Carpenter', 'One who deals with wood');
INSERT INTO `project`.`service` (`service_id`, `name`, `description`) VALUES ('2', 'Nanny', 'One who takes care of your kids');
INSERT INTO `project`.`service` (`service_id`, `name`, `description`) VALUES ('3', 'Cleaning', 'One who clears your mess');
INSERT INTO `project`.`service` (`service_id`, `name`, `description`) VALUES ('4', 'Plumbing', 'One who fixes your taps');
INSERT INTO `project`.`small_business` (`sm_id`, `email`, `password`, `street_address`, `city`, `state`, `zip`, `mobile`, `name`, `activate`) VALUES ('1', 'business1@business1.com', 'business123', 'One washington square', 'San Jose', 'CA', '95113', '1231231234', 'Business1Nanny', '1');
INSERT INTO `project`.`small_business` (`sm_id`, `email`, `password`, `street_address`, `city`, `state`, `zip`, `mobile`, `name`, `activate`) VALUES ('2', 'business1@business1.com', 'business123', 'One washington square', 'San Jose', 'CA', '95113', '2342342345', 'Business2', '0');

INSERT INTO `project`.`worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('1', '1', 'abc123', 'Capt. Piccard');
INSERT INTO `project`.`worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('2', '1', 'abc123', 'James Kirk');
INSERT INTO `project`.`worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('3', '1', 'abc123', 'Leonard McCoy');
INSERT INTO `project`.`worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('4', '1', 'abc123', 'Hikara Sulu');
INSERT INTO `project`.`worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('5', '2', 'bcd123', 'Darth Vader');
INSERT INTO `project`.`worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('6', '2', 'bcd123', 'Luke Skywalker');
INSERT INTO `project`.`worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('7', '2', 'bcd123', 'Princess Leia');
INSERT INTO `project`.`worker` (`worker_id`, `sm_id`, `password`, `name`) VALUES ('8', '2', 'bcd123', 'Han Solo');

INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('1', '1', '1');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('5', '2', '1');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('2', '1', '2');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('3', '1', '2');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('6', '2', '2');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('7', '2', '3');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('4', '1', '4');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('1', '1', '4');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('5', '2', '4');
INSERT INTO `project`.`service_provider` (`worker_id`, `sm_id`, `service_id`) VALUES ('8', '2', '4');

INSERT INTO `project`.`service_recipient` (`email`, `name`, `password`, `street_address`, `city`, `state`, `zip`, `mobile`) VALUES ('hayden@christensen.com', 'Hayden Christensen', 'hay123', 'Death Star', 'San Jose', 'CA', '95112', '4564564567');
INSERT INTO `project`.`service_recipient` (`email`, `name`, `password`, `street_address`, `city`, `state`, `zip`, `mobile`) VALUES ('natalie.portman@portman.com', 'Natalie Portman', 'nat123', 'Galactic Senate', 'San Jose', 'CA', '95112', '5675675678');
INSERT INTO `project`.`service_recipient` (`email`, `name`, `password`, `street_address`, `city`, `state`, `zip`, `mobile`) VALUES ('wil.wheaton@wheaton.com', 'Will Wheaton', 'wil123', 'United Federation of Planets', 'San Jose', 'CA', '95112', '6786786789');
INSERT INTO `project`.`service_recipient` (`email`, `name`, `password`, `street_address`, `city`, `state`, `zip`, `mobile`) VALUES ('leonard.nimoy@nimoy.com', 'Leanord Nimoy', 'leo123', 'Vulcan Government', 'San Jose', 'CA', '95112', '7897897890');

INSERT INTO `project`.`service_record` (`record_id`, `worker_id`, `sm_id`, `slot_id`, `service_id`, `service_recipient`, `service_status`) VALUES ('1', '1', '1', '1', '1', '1', '1');